# Pytchout

An all-encompassing package for baseball analytics.

## Installation

The `pytchout` package can be installed via pip:

`pip install oura-ring`

## Lookup

The `lookup` module can make queries to get player user ID information for different sources.

```python
import pytchout.lookup as lu

# Get MLB Advanced Media ID - default source
bonds_mlbam = lu.player_name_search("Bonds", "Barry")

# Get FanGraphs ID
bonds_fangraphs = lu.player_name_search("Bonds", "Barry", source="fangraphs")

# Get BaseballReference ID
bonds_bbref = lu.player_name_search("Bonds", "Barry", source="bbref")

# Get player information from ID
mystery_player = lu.player_id_search("clemero02", source="bbref")
```

## Statcast

The `statcast` module can make queries to the Statcast database hosted on [Baseball Savant](https://baseballsavant.mlb.com/statcast_search).

```python
import pytchout.statcast as sc
import pytchout.lookup as lu

# Get data for an individual pitcher within a date range
verlander_id = lu.player_name_search("Verlander", "Justin")
verlander_2019 = sc.get_pitcher(verlander_id, "2019-01-01", "2019-12-31")

# Get data for an individual batter within a date range
trout_id = lu.player_name_search("Trout", "Mike")
trout_2021 = sc.get_batter(trout_id, "2021-01-01", "2021-12-31")

# Get all pitch-by-pitch data for a season
season_data_2022 = sc.get_season("2022")

# Get all pitch-by-pitch data for a certain date range
april_2017 = sc.get_dates("2017-04-01", "2017-04-30")
```

## Analysis

The `analysis` module contains functions to do analysis on pitch-by-pitch data.

The `nathan_calculations` function adds rows to a pitch-by-pitch DataFrame to add numbers that aren't included in normal Statcast data, including `induced_vert_break`, `tilt`, `spin_efficiency`, and `gyro_angle`, among others.

The `pitcher_arsenal` function creates an overview for each pitcher in a given dataset of their pitching arsenals.
