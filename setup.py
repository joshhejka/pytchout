from setuptools import setup

setup(
    name="pytchout",
    version="0.1",
    description="Baseball Related Tools",
    url="https://gitlab.com/joshhejka/pytchout",
    author="Josh Hejka",
    author_email="jrhejka@gmail.com",
    license="MIT",
    packages=["pytchout"],
    zip_safe=False,
)
