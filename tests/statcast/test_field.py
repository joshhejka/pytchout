import pytest

from pytchout.statcast import field as fd


@pytest.fixture
def choice_field_params():
    return {
        "name": "name",
        "slug": "slug",
        "choices": [
            {"name": "choice_name", "slug": "choice_slug", "frequency": 0.25},
            {"name": "second_choice", "slug": "second_slug"},
            {"name": "replace_choice", "slug": "has..periods"},
            {"name": "integer_choice", "slug": "100", "frequency": 0.10},
        ],
        "aliases": {"alias": ["choice_slug", "second_slug"]},
        "default_choice": "choice_slug",
        "default_frequency": 0.80,
    }


@pytest.fixture
def field_params():
    return {"name": "name", "slug": "slug"}


@pytest.fixture
def metric_field_params():
    return {
        "name": "name",
        "slug": "slug",
        "choices": [
            {"name": "choice_name", "slug": "choice_slug", "min_value": 0, "max_value": 100},
            {"name": "second_choice", "slug": "second_slug", "min_value": None, "max_value": 100},
            {"name": "replace_choice", "slug": "has..periods", "min_value": 0, "max_value": None},
            {"name": "integer_choice", "slug": "100", "min_value": None, "max_value": None},
        ],
        "aliases": {"alias": ["choice_slug", "second_slug"]},
        "default_choice": "choice_slug",
    }


@pytest.fixture
def ssf(choice_field_params):
    return fd.SingleSelectField(**choice_field_params)


@pytest.fixture
def msf(choice_field_params):
    return fd.MultiSelectField(**choice_field_params)


@pytest.fixture
def drf(field_params):
    return fd.DateRangeField(**field_params)


@pytest.fixture
def plf(field_params):
    return fd.PlayerLookupField(**field_params)


@pytest.fixture
def mrf(metric_field_params):
    return fd.MetricRangeField(**metric_field_params)


@pytest.fixture
def aif(field_params):
    return fd.AsIsField(**field_params)


def test_choice_init():
    c = fd.Choice(slug="slug", name="name", min_value=0, max_value=100)

    assert c.slug == "slug"
    assert c.name == "name"
    assert c.options == {"slug", "name"}
    assert c.min_value == 0
    assert c.max_value == 100


def test_field_abc_init():
    with pytest.raises(TypeError):
        fd.Field(name="name", slug="slug")


class TestChoiceField:
    def test_choice_field_abc_init(self):
        with pytest.raises(TypeError):
            fd.ChoiceField(name="name", slug="slug", choices=[{"name": "choice_name", "slug": "choice_slug"}])

    def test_choice_field_init(self, ssf):
        assert ssf.name == "name"
        assert ssf.slug == "slug"
        assert isinstance(ssf.choices, list)
        assert all(isinstance(choice, fd.Choice) for choice in ssf.choices)
        assert ssf.default_choice == "choice_slug"
        assert ssf.default_frequency == 0.80

    def test_get_choice_from_value(self, ssf):
        choice = ssf._get_choice_from_value("choice_name")

        assert isinstance(choice, fd.Choice)
        assert choice.name == "choice_name"
        assert choice.slug == "choice_slug"

        choice = ssf._get_choice_from_value("choice_slug")

        assert isinstance(choice, fd.Choice)
        assert choice.name == "choice_name"
        assert choice.slug == "choice_slug"

    def test_get_choice_from_value_invalid_value(self, ssf):
        with pytest.raises(ValueError):
            ssf._get_choice_from_value("invalid_value")

    def test_guide(self, ssf):
        assert ssf.guide() == {
            "choice_name": "choice_slug",
            "replace_choice": "has..periods",
            "second_choice": "second_slug",
            "integer_choice": "100",
        }


class TestSingleSelectField:
    def test_get_params(self, ssf):
        assert ssf.get_params("choice_name") == {"slug": "choice_slug"}
        assert ssf.get_params("choice_slug") == {"slug": "choice_slug"}
        assert ssf.get_params("replace_choice") == {"slug": r"has\.\.periods"}
        assert ssf.get_params("100") == {"slug": "100"}
        assert ssf.get_params(100) == {"slug": "100"}

    def test_get_params_multiple_choices(self, ssf):
        with pytest.raises(ValueError):
            ssf.get_params(["choice_name", "second_choice"])

    def test_get_params_invalid_value(self, ssf):
        with pytest.raises(ValueError):
            ssf.get_params("invalid_value")

        with pytest.raises(ValueError):
            ssf.get_params(["second_choice"])

    def test_get_frequency_factor(self, ssf):
        assert ssf.get_frequency_factor({"slug": "choice_slug"}) == 0.25
        assert ssf.get_frequency_factor({"slug": "second_slug"}) == 0.80


class TestMultiSelectField:
    def test_get_params(self, msf):
        assert msf.get_params("choice_name") == {"slug": "choice_slug|"}
        assert msf.get_params(["choice_name", "second_choice"]) == {"slug": "choice_slug|second_slug|"}
        assert msf.get_params(["second_choice", "choice_name"]) == {"slug": "choice_slug|second_slug|"}
        assert msf.get_params(("choice_name", "second_choice")) == {"slug": "choice_slug|second_slug|"}
        assert msf.get_params("replace_choice") == {"slug": r"has\.\.periods|"}

    def test_aliases(self, msf):
        assert msf.get_params("alias") == {"slug": "choice_slug|second_slug|"}

    def test_get_frequency_factor(self, msf):
        assert msf.get_frequency_factor({"slug": "choice_slug|"}) == 0.25
        assert msf.get_frequency_factor({"slug": "choice_slug|100|"}) == 0.35
        assert msf.get_frequency_factor({"slug": "choice_slug|second_slug|"}) == 1.0


class TestDateRangeField:
    def test_get_params(self, drf):
        assert drf.get_params("2019-05-01") == {"slug_gt": "2019-05-01", "slug_lt": "2019-05-01"}
        assert drf.get_params(["2019-05-01", "2019-06-01"]) == {
            "slug_gt": "2019-05-01",
            "slug_lt": "2019-06-01",
        }
        assert drf.get_params(("2019-05-01", "2019-06-01")) == {
            "slug_gt": "2019-05-01",
            "slug_lt": "2019-06-01",
        }
        assert drf.get_params(["2019-05-01", None]) == {"slug_gt": "2019-05-01", "slug_lt": ""}
        assert drf.get_params(None) == {"slug_gt": "", "slug_lt": ""}

    def test_get_params_too_many_dates(self, drf):
        with pytest.raises(ValueError):
            drf.get_params(["2019-05-01", "2019-05-01", "2019-05-01"])


class TestPlayerLookupField:
    def test_get_params(self, plf):
        assert plf.get_params("545333") == {"slug": ["545333"]}
        assert plf.get_params(545333) == {"slug": ["545333"]}
        assert plf.get_params(["545333"]) == {"slug": ["545333"]}
        assert plf.get_params([545333]) == {"slug": ["545333"]}
        assert plf.get_params(("545333",)) == {"slug": ["545333"]}
        assert plf.get_params((545333,)) == {"slug": ["545333"]}
        assert plf.get_params(["123456", "545333"]) == {"slug": ["123456", "545333"]}
        assert plf.get_params(["545333", "123456"]) == {"slug": ["123456", "545333"]}

    def test_get_params_invalid_id(self, plf):
        with pytest.raises(ValueError):
            plf.get_params(1234567)

    def test_get_params_no_id(self, plf):
        with pytest.raises(ValueError):
            plf.get_params(None)

    def test_get_frequency_factor(self, plf):
        assert plf.get_frequency_factor({"slug": ["123456", "545333"]}) == 0.02
        assert plf.get_frequency_factor({"slug": ["123456"] * 200}) == 1.0


class TestMetricRangeField:
    def test_get_params(self, mrf):
        assert mrf.get_params({"choice_name": [10, 50]}) == {
            "slug_1": "choice_slug",
            "slug_1_gt": "10",
            "slug_1_lt": "50",
        }
        assert mrf.get_params({"choice_slug": [10, 50]}) == {
            "slug_1": "choice_slug",
            "slug_1_gt": "10",
            "slug_1_lt": "50",
        }
        assert mrf.get_params({"choice_slug": (10, 50)}) == {
            "slug_1": "choice_slug",
            "slug_1_gt": "10",
            "slug_1_lt": "50",
        }
        assert mrf.get_params({"choice_slug": [-10, 110]}) == {
            "slug_1": "choice_slug",
            "slug_1_gt": "0",
            "slug_1_lt": "100",
        }
        assert mrf.get_params({"choice_slug": [10.0, 50.0]}) == {
            "slug_1": "choice_slug",
            "slug_1_gt": "10",
            "slug_1_lt": "50",
        }
        assert mrf.get_params({"choice_slug": 45}) == {
            "slug_1": "choice_slug",
            "slug_1_gt": "45",
            "slug_1_lt": "45",
        }
        assert mrf.get_params({"choice_slug": "45"}) == {
            "slug_1": "choice_slug",
            "slug_1_gt": "45",
            "slug_1_lt": "45",
        }
        assert mrf.get_params({"choice_name": [10, 50], "second_choice": ["40", "60"]}) == {
            "slug_1": "choice_slug",
            "slug_1_gt": "10",
            "slug_1_lt": "50",
            "slug_2": "second_slug",
            "slug_2_gt": "40",
            "slug_2_lt": "60",
        }

    def test_get_params_invalid_metric_name(self, mrf):
        with pytest.raises(ValueError):
            mrf.get_params({"invalid_choice": [10, 50]})

    def test_get_params_too_many_value(self, mrf):
        with pytest.raises(ValueError):
            mrf.get_params({"choice_name": [10, 50, 100]})

    def test_get_frequency_factor(self, mrf):
        assert mrf.get_frequency_factor({"slug_1": "choice_slug", "slug_1_gt": "10", "slug_1_lt": "50",}) == 1.0


class TestAsIsField:
    def test_get_params(self, aif):
        assert aif.get_params("hello") == {"slug": "hello"}
        assert aif.get_params(17) == {"slug": "17"}
        assert aif.get_params("17") == {"slug": "17"}
        assert aif.get_params("123456.0") == {"slug": "123456.0"}

    def test_get_frequency_factor(self, aif):
        assert aif.get_frequency_factor({"slug": "hello"}) == 0.001
