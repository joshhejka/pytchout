import pandas as pd

from pytchout.statcast import get_game
from pytchout.analysis import nathan as nt


def test_nathan():
    data = get_game(567567)
    assert isinstance(nt.nathan_calculations(data), pd.DataFrame)
