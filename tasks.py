from invoke import task


@task
def test(c):
    c.run("pytest", pty=True)


@task
def cov(c):
    c.run("coverage run -m pytest", pty=True)
    c.run("coverage report -m")


@task
def flake(c, location="."):
    c.run(f"flake8 {location}")


@task
def lint(c, location="."):
    c.run(f"pylint {location}")


@task
def black(c, location="."):
    c.run(f"black {location}")


@task
def install(c):
    c.run("poetry install")


@task
def format(c):
    black(c)
    flake(c)
    lint(c)
