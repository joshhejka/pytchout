import pandas as pd

from pytchout.analysis.nathan import _decimal_tilt_to_tilt

# from pytchout.statcast.statcast import get_pitcher, get_dates, query


_arsenal_columns = [
    "release_speed",
    "release_spin_rate",
    "induced_horz_break",
    "induced_vert_break",
    "decimal_tilt",
    "spin_direction",
    "spin_efficiency",
    "gyro_angle",
    "release_pos_x",
    "release_pos_z",
    "release_extension",
    "release_angle",
]


#
# PUBLIC
#


# TODO: See if it's possible to account for where pitcher stands on the mound and see how that affects release angle.
# TODO: "player_name" might not be pitcher but still want it to work.
# TODO: Delete players who aren't primary pitchers.
# TODO: Remove intentional balls.
def pitcher_arsenal(data: pd.DataFrame, round_to: int = 1) -> pd.DataFrame:
    group_by = ["pitcher", "player_name", "game_year", "p_throws", "pitch_type"]

    grouped = data.groupby(group_by)

    means = (
        (
            data.groupby(["pitcher", "player_name", "game_year"])
            .count()[["pitch_type"]]
            .rename({"pitch_type": "total_pitches"}, axis=1)
        )
        .join(grouped[_arsenal_columns].mean())
        .reorder_levels(group_by)
    )

    means["spin_efficiency"] *= 100

    means.insert(0, column="number_thrown", value=grouped["pitch_type"].count())

    means.insert(2, column="percent_thrown", value=(means["number_thrown"] / means["total_pitches"] * 100))

    means.insert(5, column="bauer_units", value=(means["release_spin_rate"] / means["release_speed"]))

    # TODO: Confirm that initial DataFrame has nathan columns.
    means.insert(9, column="tilt", value=_decimal_tilt_to_tilt(means["decimal_tilt"].dropna()))

    means = means.round(round_to)
    means["release_spin_rate"] = means["release_spin_rate"].round(0).astype(pd.Int64Dtype())

    return means


if __name__ == "__main__":
    pass
    # data = get_pitcher("545333", nathan=True)

    # res = get_pitcher([player_name_search("Verlander", "Justin"), 545333], "2018-01-01", "2019-12-31", nathan=True)

    # res = get_dates("2019-04-01", "2019-04-03", nathan=True)

    # pd.options.display.max_columns = None
    # pd.options.display.max_rows = None
    # ic(data)

    # ars = pitcher_arsenal(res, round_to=2)
    # ic(ars)
    # ars.to_csv("test_arsenal_again.csv")

    # res = res.groupby(["player_name", "game_year", "p_throws", "stand", "pitch_type"])
    # ic(res.mean())
    # res.mean().to_csv("test_arsenal.csv")
