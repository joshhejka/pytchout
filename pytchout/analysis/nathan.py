import numpy as np
import pandas as pd
from typing import List

from icecream import ic
from loguru import logger

from pytchout.statcast import statcast as sc
from pytchout.utils import timeit


# Environmental constant
K: float = 0.005383

# Standard gravity in ft/s^2
G: float = 32.174

# Distance of the pitcher's mound in feet
MOUND_DISTANCE: float = 60.5

# Distance of the initial Statcast measurement in feet
STATCAST_INITIAL_MEASUREMENT: float = 50.0

# Length of the plate in feet
PLATE_LENGTH: float = 17.0 / 12.0

# Number of inches in a foot
INCHES_PER_FOOT: float = 12.0

# Theoretical height of a parallel release point
ECKERSLEY_LINE: float = 2.85


#
# PUBLIC
#


# TODO: Add documentation of new fields (with units).
def nathan_calculations(data: pd.DataFrame) -> pd.DataFrame:
    columns: List[str] = [
        "release_extension",
        "release_spin_rate",
        "release_pos_x",
        "release_pos_z",
        "vx0",
        "vy0",
        "vz0",
        "ax",
        "ay",
        "az",
        "plate_x",
        "plate_z",
    ]

    nathan: pd.DataFrame = data[columns].dropna()

    nathan = _calculate_release_point_y(nathan)
    nathan = _calculate_release_time(nathan)
    nathan = _calculate_velocity_components_at_release(nathan)
    nathan = _calculate_flight_time(nathan)
    nathan = _calculate_induced_movement(nathan)
    nathan = _calculate_average_velocity_components(nathan)
    nathan = _calculate_average_velocity(nathan)
    nathan = _calculate_average_drag(nathan)
    nathan = _calculate_magnus_acceleration_components(nathan)
    nathan = _calculate_magnus_acceleration(nathan)
    nathan = _calculate_magnus_movement(nathan)
    nathan = _calculate_drag_coefficient(nathan)
    nathan = _calculate_lift_coefficient(nathan)
    nathan = _calculate_spin_factor(nathan)
    nathan = _calculate_transverse_spin(nathan)
    nathan = _calculate_transverse_spin_components(nathan)
    nathan = _calculate_phi(nathan)
    nathan = _calculate_tilt_and_spin_direction(nathan)
    nathan = _calculate_spin_efficiency(nathan)
    nathan = _calculate_gyro_angle(nathan)
    # TODO: This should be done even if other stuff is None. Should maybe go first?
    nathan = _calculate_release_angle(nathan)

    # TODO: Add Bauer units
    data[
        [
            "induced_horz_break",
            "induced_vert_break",
            "decimal_tilt",
            "tilt",
            "spin_direction",
            "spin_efficiency",
            "gyro_angle",
            "release_angle",
        ]
    ] = nathan[
        ["x_mvt", "z_mvt", "decimal_tilt", "tilt", "spin_direction", "spin_efficiency", "theta", "release_angle"]
    ].copy()

    return data


#
# PRIVATE
#


def _calculate_release_point_y(data: pd.DataFrame) -> pd.DataFrame:
    release_extension = data["release_extension"]

    data["release_pos_y"] = MOUND_DISTANCE - release_extension

    return data


def _calculate_release_time(data: pd.DataFrame) -> pd.DataFrame:
    vy0 = data["vy0"]
    ay = data["ay"]
    release_pos_y = data["release_pos_y"]

    data["tR"] = _time_in_air(vy0, ay, release_pos_y, STATCAST_INITIAL_MEASUREMENT, False)

    return data


def _calculate_velocity_components_at_release(data: pd.DataFrame) -> pd.DataFrame:
    vx0 = data["vx0"]
    vy0 = data["vy0"]
    vz0 = data["vz0"]

    ax = data["ax"]
    ay = data["ay"]
    az = data["az"]

    tR = data["tR"]

    data["vxR"] = vx0 + (ax * tR)
    data["vyR"] = vy0 + (ay * tR)
    data["vzR"] = vz0 + (az * tR)

    return data


def _calculate_flight_time(data: pd.DataFrame) -> pd.DataFrame:
    vyR = data["vyR"]
    ay = data["ay"]
    release_pos_y = data["release_pos_y"]

    data["tf"] = _time_in_air(vyR, ay, release_pos_y, PLATE_LENGTH, True)

    return data


def _calculate_induced_movement(data: pd.DataFrame) -> pd.DataFrame:
    plate_x = data["plate_x"]
    plate_z = data["plate_z"]

    vxR = data["vxR"]
    vyR = data["vyR"]
    vzR = data["vzR"]

    release_pos_x = data["release_pos_x"]
    release_pos_y = data["release_pos_y"]
    release_pos_z = data["release_pos_z"]

    tf = data["tf"]

    data["x_mvt"] = -((plate_x - release_pos_x - (vxR / vyR) * (PLATE_LENGTH - release_pos_y)) * INCHES_PER_FOOT)
    data["z_mvt"] = (
        plate_z - release_pos_z - (vzR / vyR) * (PLATE_LENGTH - release_pos_y) + (0.5 * G * (tf ** 2))
    ) * INCHES_PER_FOOT

    return data


def _calculate_average_velocity_components(data: pd.DataFrame) -> pd.DataFrame:
    vxR = data["vxR"]
    vyR = data["vyR"]
    vzR = data["vzR"]

    ax = data["ax"]
    ay = data["ay"]
    az = data["az"]

    tf = data["tf"]

    data["vxbar"] = ((2 * vxR) + (ax * tf)) / 2
    data["vybar"] = ((2 * vyR) + (ay * tf)) / 2
    data["vzbar"] = ((2 * vzR) + (az * tf)) / 2

    return data


def _calculate_average_velocity(data: pd.DataFrame) -> pd.DataFrame:
    vxbar = data["vxbar"]
    vybar = data["vybar"]
    vzbar = data["vzbar"]

    data["vbar"] = _n_component_mean(vxbar, vybar, vzbar)

    return data


def _calculate_average_drag(data: pd.DataFrame) -> pd.DataFrame:
    ax = data["ax"]
    ay = data["ay"]
    az = data["az"]

    vxbar = data["vxbar"]
    vybar = data["vybar"]
    vzbar = data["vzbar"]

    vbar = data["vbar"]

    data["adrag"] = -((ax * vxbar) + (ay * vybar) + ((az + G) * vzbar)) / vbar

    return data


def _calculate_magnus_acceleration_components(data: pd.DataFrame) -> pd.DataFrame:
    ax = data["ax"]
    ay = data["ay"]
    az = data["az"]

    vxbar = data["vxbar"]
    vybar = data["vybar"]
    vzbar = data["vzbar"]

    adrag = data["adrag"]

    vbar = data["vbar"]

    data["amagx"] = ax + (adrag * vxbar / vbar)
    data["amagy"] = ay + (adrag * vybar / vbar)
    data["amagz"] = az + (adrag * vzbar / vbar) + G

    return data


def _calculate_magnus_acceleration(data: pd.DataFrame) -> pd.DataFrame:
    amagx = data["amagx"]
    amagy = data["amagy"]
    amagz = data["amagz"]

    data["amag"] = _n_component_mean(amagx, amagy, amagz)

    return data


def _calculate_magnus_movement(data: pd.DataFrame) -> pd.DataFrame:
    amagx = data["amagx"]
    amagz = data["amagz"]

    tf = data["tf"]

    data["Mx"] = 0.5 * amagx * (tf ** 2) * INCHES_PER_FOOT
    data["Mz"] = 0.5 * amagz * (tf ** 2) * INCHES_PER_FOOT

    return data


def _calculate_drag_coefficient(data: pd.DataFrame) -> pd.DataFrame:
    adrag = data["adrag"]
    vbar = data["vbar"]

    data["Cd"] = adrag / ((vbar ** 2) * K)

    return data


def _calculate_lift_coefficient(data: pd.DataFrame) -> pd.DataFrame:
    amag = data["amag"]
    vbar = data["vbar"]

    data["Cl"] = amag / ((vbar ** 2) * K)

    return data


def _calculate_spin_factor(data: pd.DataFrame) -> pd.DataFrame:
    Cl = data["Cl"]

    # data["S"] = 0.4 * Cl / (1 - (2.32 * Cl))

    Cl_adj = Cl.where(Cl < 0.336, np.nan)

    data["S"] = 0.1666 * np.log(0.336 / (0.336 - Cl_adj))

    return data


def _calculate_transverse_spin(data: pd.DataFrame) -> pd.DataFrame:
    S = data["S"]
    vbar = data["vbar"]

    data["spinT"] = 78.92 * S * vbar

    return data


def _calculate_transverse_spin_components(data: pd.DataFrame) -> pd.DataFrame:
    spinT = data["spinT"]

    vxbar = data["vxbar"]
    vybar = data["vybar"]
    vzbar = data["vzbar"]

    amagx = data["amagx"]
    amagy = data["amagy"]
    amagz = data["amagz"]

    vbar = data["vbar"]

    amag = data["amag"]

    data["spinTx"] = spinT * ((vybar * amagz) - (vzbar * amagy)) / (amag * vbar)
    data["spinTy"] = spinT * ((vzbar * amagx) - (vxbar * amagz)) / (amag * vbar)
    data["spinTz"] = spinT * ((vxbar * amagy) - (vybar * amagx)) / (amag * vbar)

    return data


def _calculate_phi(data: pd.DataFrame) -> pd.DataFrame:
    amagx = data["amagx"]
    amagz = data["amagz"]

    arctan = np.arctan2(amagz, -amagx)

    # TODO: Double check that this applies to the correct rows
    data["phi"] = np.where(amagz > 0, arctan * 180 / np.pi, 360 + arctan * 180 / np.pi)

    return data


def _calculate_tilt_and_spin_direction(data: pd.DataFrame) -> pd.DataFrame:
    phi = data["phi"]

    decimal_tilt = 3 - (1 / 30) * phi
    # decimal_tilt_adj = decimal_tilt.where(decimal_tilt <= 0, decimal_tilt + 12, decimal_tilt)

    decimal_tilt_adj = decimal_tilt.where(decimal_tilt > 0, decimal_tilt + 12)

    tilt = _decimal_tilt_to_tilt(decimal_tilt_adj)
    spin_direction = _tilt_to_spin_direction(tilt)

    data["decimal_tilt"] = decimal_tilt_adj
    data["tilt"] = tilt
    data["spin_direction"] = spin_direction

    return data


# TODO: Adjust spin efficiency to match distribution found on "active spin" leaderboards.
def _calculate_spin_efficiency(data: pd.DataFrame) -> pd.DataFrame:
    spinT = data["spinT"]
    release_spin_rate = data["release_spin_rate"]

    data["spin_efficiency"] = spinT / release_spin_rate

    return data


def _calculate_gyro_angle(data: pd.DataFrame) -> pd.DataFrame:
    spin_efficiency = data["spin_efficiency"]

    # TODO: Double check that these apply to the correct rows
    spin_efficiency_adj = np.where((spin_efficiency >= -1.0) & (spin_efficiency <= 1.0), spin_efficiency, -1.0)

    data["theta"] = np.where(spin_efficiency_adj >= 0, (np.arccos(spin_efficiency_adj) * 180) / np.pi, np.nan)

    return data


# TODO: Should technically be separate because it doesn't rely on all the other fields
def _calculate_release_angle(data: pd.DataFrame) -> pd.DataFrame:
    release_pos_x = data["release_pos_x"]
    release_pos_z = data["release_pos_z"]

    data["release_angle"] = np.degrees(np.arctan2((release_pos_z - ECKERSLEY_LINE), abs(release_pos_x)))

    return data


#
# HELPER
#


def _time_in_air(velocity, acceleration, position, adjustment: float, positive: bool = True) -> pd.Series:
    if positive:
        direction_factor = 1
    else:
        direction_factor = -1

    return (
        -velocity - np.sqrt((velocity ** 2) - (2 * acceleration * (direction_factor * (position - adjustment))))
    ) / acceleration


def _n_component_mean(*args) -> pd.Series:
    return np.sqrt(sum([component ** 2 for component in args]))


def _decimal_tilt_to_tilt(decimal_tilt: pd.Series, minutes_round: int = 1) -> pd.Series:
    hours: pd.Series = pd.to_numeric(np.floor(decimal_tilt), downcast="integer")
    minutes: pd.Series = minutes_round * np.round((decimal_tilt * 60 % 60) / minutes_round).astype(int)

    hours_adj: pd.Series = hours.where(minutes != 60, hours + 1) % 12
    minutes_adj: pd.Series = minutes.where(minutes != 60, 0)

    minutes_formatted: pd.Series = minutes_adj.apply("{:0>2}".format)

    return hours_adj.astype(str) + ":" + minutes_formatted.astype(str)


def _tilt_to_spin_direction(tilt: pd.Series) -> pd.Series:
    tilt_time: pd.Series = pd.to_datetime(tilt, format="%H:%M")

    hours: pd.Series = tilt_time.dt.hour
    minutes: pd.Series = tilt_time.dt.minute

    return ((hours * 30) + (minutes / 2) + 180) % 360


if __name__ == "__main__":
    # test_df = [1.5, 2.75, 3.90, 4.99, 6.05, 11.99, 13]
    # df = pd.DataFrame(test_df)
    # ic(df)
    # for i in range(10000):
    # tilt = ic(_decimal_tilt_to_tilt(df[0], minutes_round=1))
    # ic(_tilt_to_spin_direction(tilt))

    bauer = sc.get_pitcher("545333", "2019-05-01", "2019-06-01", aggregate=False, nathan=False)

    # pd.options.display.max_columns = None
    # ic(bauer)
    # nathan_calculations(bauer)
    # ic(bauer["release_extension"])
    # ic(type(bauer["release_extension"]))
