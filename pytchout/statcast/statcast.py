"""
Make requests to the Statcast database.

This module provides a set of functions that allow for querying and downloading data from the Baseball Savant Statcast database.

Examples:
    from pytchout import statcast as sc

    # This will get all of Trevor Bauer's pitches in the 2019 season.
    trevor_bauer = sc.get_pitcher(545333, 2019-01-01, 2019-12-31)
"""

from datetime import datetime
from typing import Dict, List, Union

import pandas as pd
from icecream import ic
from loguru import logger

from pytchout.analysis import nathan as nt
from pytchout.analysis import arsenal as ar
from pytchout.statcast.query import make_request
from pytchout.statcast.types import FieldValue


# TODO: Allow for either season or date input and have helper method sort it out.
# TODO: Maybe do all "get seasons from dates" logic in query?
# TODO: Add "params" arg for additional parameters?


#
# PUBLIC
#


def get_season(year: Union[str, int], *, nathan: bool = False):
    """
    Download data for one season.

    Args:
        year: The year to get data for.
        aggregate: Whether to aggregate the data and return a single row of the averages.
        nathan: Whether to perform spin rate and spin efficiency calculations on the data.

    Returns:
        A DataFrame containing pitch-by-pitch data for the given year.
    """
    return query({"season": year}, nathan=nathan)


def get_dates(start_date: str, end_date: str, *, nathan: bool = False) -> pd.DataFrame:
    return query({"game_date_range": [start_date, end_date]}, nathan=nathan)


def get_game(game_pk: Union[str, int], *, nathan: bool = False) -> pd.DataFrame:
    return query({"game_pk": game_pk}, nathan=nathan)


def get_pitcher(
    pitcher_id: FieldValue, start_date: str = "2019-01-01", end_date: str = "2019-12-31", *, nathan: bool = False,
):
    """
    Download data for a pitcher between given dates.

    Args:
        pitcher_id: The pitcher to get data for.
        start_date: The first date to get data for.
        end_date: The last date to get data for.
        aggregate: Whether to aggregate the data and return a single row of the averages.
        nathan: Whether to perform spin rate and spin efficiency calculations on the data.

    Returns:
        A DataFrame containing pitch-by-pitch data for the given pitcher between the given dates.
    """
    return query(
        {
            "pitchers": pitcher_id,
            "game_date_range": [start_date, end_date],
            "season": _get_seasons_from_dates(start_date, end_date),
        },
        nathan=nathan,
    )


def get_batter(
    batter_id: FieldValue, start_date: str = "2019-01-01", end_date: str = "2019-12-31", *, nathan: bool = False,
):
    """
    Download data for a batter between given dates.

    Args:
        batter_id: The batter to get data for.
        start_date: The first date to get data for.
        end_date: The last date to get data for.
        aggregate: Whether to aggregate the data and return a single row of the averages.
        nathan: Whether to perform spin rate and spin efficiency calculations on the data.

    Returns:
        A DataFrame containing pitch-by-pitch data for the given batter between the given dates.
    """
    return query(
        {
            "batters": batter_id,
            "game_date_range": [start_date, end_date],
            "season": _get_seasons_from_dates(start_date, end_date),
        },
        nathan=nathan,
    )


def get_matchup(
    pitcher_id: FieldValue,
    batter_id: FieldValue,
    start_date: str = "2019-01-01",
    end_date: str = "2019-12-31",
    *,
    nathan: bool = False,
) -> pd.DataFrame:
    return query(
        {
            "pitchers": pitcher_id,
            "batters": batter_id,
            "game_date_range": [start_date, end_date],
            "season": _get_seasons_from_dates(start_date, end_date),
        },
        nathan=nathan,
    )


def get_team(
    team: str, start_date: str = "2019-01-01", end_date: str = "2019-12-31", *, nathan: bool = False,
) -> pd.DataFrame:
    return query(
        {
            "team": team,
            "game_date_range": [start_date, end_date],
            "season": _get_seasons_from_dates(start_date, end_date),
        },
        nathan=nathan,
    )


# TODO: Move to own file.
def get_leaderboard(leaderboard: str, params: Dict) -> pd.DataFrame:
    pass


# @log_exception
@logger.catch()
def query(params: Dict, *, nathan: bool = False) -> pd.DataFrame:
    """
    Download data for a given set of query parameters.

    Args:
        params: A mapping of field names to values to include in the query.
        aggregate: Whether to aggregate the data and return a single row of the averages.
        nathan: Whether to perform spin rate and spin efficiency calculations on the data.

    Returns:
        A DataFrame containing pitch-by-pitch data that meets the query criteria.
    """
    # if not aggregate:
    #     params["type"] = "details"
    data = make_request(params)
    return nt.nathan_calculations(data) if nathan else data


#
# PRIVATE
#


def _get_seasons_from_dates(start_date: str, end_date: str) -> List[str]:
    """
    Return a list of years given a start date and an end date. Queries on Baseball Savant default to the 2019 season so this function is used when a query has a start date and end date to make sure that the season field will be set correctly.

    Args:
        start_date: The first date to consider.
        end_date: The last date to consider.

    Returns:
        A list of years between the two given dates.

    Raise:
        ValueError: If an invalid date is given.
    """
    try:
        start_year: int = datetime.strptime(start_date, "%Y-%m-%d").year
        end_year: int = datetime.strptime(end_date, "%Y-%m-%d").year

        return [*range(start_year, end_year + 1)]

    except ValueError:
        raise ValueError("Invalid date format. Use YYYY-MM-DD.")


if __name__ == "__main__":
    # ic(player_name_search("bauer", "trevor"))
    # ic(player_id_search(545333))

    # sys.exit(0)

    single = {
        # "pitchers": ["545333", "434378", 123456],
        "pitchers": ["545333"],
        # "batters": "123456",
        # "venue": 2395,
        # "venue": 2395,
        "season": [2017, 2018, 2019],
        # "game_date_range": ["2018-07-28", "2019-06-09"],
        # "game_date_range": [None, "2019-06-09"],
        "pitch_type": ["fastballs"],
        # "player_type": "batter",
        "sort_order": "asc",
        "pa_result": "double",
        # If metric range is None, then set to max/min
        "metric_range": {"pitch velocity": [None, 100.0]},
    }

    multipart = {"season": [2017, 2018, 2019], "venue": 2395}

    season = {"season": 2019}

    # res = get_matchup(
    #     player_name_search("ODAY", "Darren"), player_name_search("Zimmermann", "Ryan"), "2010-01-01"
    # )

    # res = get_game(565444)

    # data = get_dates("2019-09-21", "2019-09-21", nathan=True)
    # ic(data.columns)

    # res = query(single)
    # nathan = nt.nathan_calculations(res)
    # ic(nathan)
    # ars = ar.pitcher_arsenal(nathan)
    # ic(ars)

    # data_2019 = get_season(2019, nathan=True)
    # data_2019.to_csv("2019_test_data_sorted.csv")

    # arsenal = ar.pitcher_arsenal(data_2019)
    # arsenal.to_csv("arsenals_2019.csv")

    df = get_pitcher("545333", "2012-01-01", "2019-12-31", nathan=True)
    ic(df)

    ic(ar.pitcher_arsenal(df))

    # data.to_csv("test_sorted_data2" ".csv")

    # jv_data = get_pitcher(player_name_search("Verlander", "Justin"), "2017-01-01", "2019-12-31", nathan=True)
    # ic(jv_data)

    # data = query(multipart, nathan=True)

    # pd.options.display.max_columns = None
    # ic(jv_data)

    # cabby = player_name_search("cabrera", "miguel")
    # ic(get_batter(cabby, start_date="2011-01-01"))

    # ic(get_pitcher(545333))
    # ic(query(season))
