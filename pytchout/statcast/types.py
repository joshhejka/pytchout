from typing import Dict, List, Tuple, Union

FieldValue = Union[str, int, List[Union[str, int]]]
ParamDict = Dict[str, FieldValue]
ValueCollection = Union[List[Union[str, int]], Tuple[Union[str, int]]]
