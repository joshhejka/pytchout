"""
Compile and make requests to Statcast database.

This module takes a given list of query parameters and validates them. Since a Statcast query will return a maximum of 40,000 rows, a rough estimate of the number of rows in the request is calculated and the request is broken up accordingly. The data is then returned as a DataFrame.

Attributes:
    MAX_ROWS_PER_REQUEST: Statcast limits queries to a maximum of 40,000 rows. Uses 35,000 to ensure there will be no accidental data loss.

    PITCHES_PER_GAME: A slight over-estimate of the number of pitches per game for the same reasons as above.
"""

import io
from datetime import datetime
from difflib import get_close_matches
from typing import IO, Any, Dict, List, Tuple

import pandas as pd
from icecream import ic
from loguru import logger

import pytchout.downloader as dl
from pytchout.statcast.base import default_params, console
from pytchout.statcast import field as fd
from pytchout.statcast.base import game_logs, search_base_url, statcast_fields
from pytchout.statcast.types import ParamDict

MAX_ROWS_PER_REQUEST: int = 35_000
PITCHES_PER_GAME: int = 325

# Will help fix some issues with creating the DataFrame. Should eventually build this up to include dtypes for all columns.
_statcast_dtypes: Dict[str, Any] = {"if_fielding_alignment": str, "of_fielding_alignment": str, "sv_id": str}

_deprecated_columns: List[str] = [
    "spin_dir",
    "spin_rate_deprecated",
    "break_angle_deprecated",
    "break_length_deprecated",
    "tfs_deprecated",
    "tfs_zulu_deprecated",
    "umpire",
]


#
# PUBLIC
#


# TODO: Automatically update seasons in request?
def make_request(params: Dict) -> pd.DataFrame:
    """
    Compile and validate query parameters, send request, and return result.

    Args:
        params: The dict of field names to values to include in the query.

    Returns:
        A DataFrame containing the pitch-by-pitch data that meets the query criteria.
    """

    # Type hinting and variable unpacking doesn't work together so need to declare variables before initial function call.
    request_params: ParamDict
    frequency_factor: float

    request_params, frequency_factor = _get_params_and_frequency_factor(params)
    games_in_request: pd.DataFrame = _get_games_in_request(request_params)
    request_date_pairs: List[Tuple[str, str]] = _get_request_date_pairs(games_in_request, frequency_factor)
    requests_to_make: List[ParamDict] = _compile_requests(request_params, request_date_pairs)
    # ic(requests_to_make)
    filenames: List[str] = _get_filenames(requests_to_make)

    console.print(
        f"Making {len(requests_to_make)} request(s). {'This may take a while.' if len(requests_to_make) > 1 else ''}"
    )

    # TODO: Check for empty data error.
    with io.StringIO() as output:
        dl.download_csv(search_base_url, output, requests_to_make, filenames)
        return _get_statcast_df(output, len(requests_to_make) > 1)


#
# PRIVATE
#


def _get_params_and_frequency_factor(params: Dict) -> (ParamDict, float):
    """
    Validate query parameters and calculate frequency factor.

    Args:
        params: The dict of field names to values to be validated.

    Returns:
        A tuple where the first element is a dict of field slugs to their validated value slugs and the second element is the estimated frequency factor of the request.

    Raises:
        InvalidField: If a given field name is not associated with a field.
        DuplicateFieldError: If a field name is provided multiple times.
    """
    request_params: ParamDict = {}
    frequency_factor: float = 1.0

    for field_name, values in params.items():
        field_object: fd.Field = statcast_fields.get(field_name)

        if not field_object:
            # TODO: Turn into helper function?
            closest_matches = ", ".join(
                f"'{cm}'" for cm in get_close_matches(field_name, statcast_fields.keys(), cutoff=0)
            )
            raise ValueError(f"'{field_name}' is not a valid field name. Did you mean {closest_matches}?")

        if request_params.get(field_object.slug):
            raise ValueError(f"'{field_name}' already in request.")

        valid_values: ParamDict = field_object.get_params(values)
        request_params.update(valid_values)

        # Only ChoiceField and PlayerLookupField objects have a get_frequency_factor method. DateRangeField objects don't because the number of games in the request is calculated directly in the _get_games_in_request module function.
        if isinstance(field_object, (fd.ChoiceField, fd.PlayerLookupField, fd.AsIsField)):
            frequency_factor *= field_object.get_frequency_factor(valid_values)

    request_params = {**default_params, **request_params}

    if not request_params.get("game_date_gt"):
        request_params["game_date_gt"] = f"{request_params.get('hfSea').split('|')[0]}-01-01"

    # TODO: Change this to get today's date if the current year is included.
    if not request_params.get("game_date_lt"):
        request_params["game_date_lt"] = f"{request_params.get('hfSea').split('|')[-2]}-12-31"

    _check_dates(request_params["game_date_gt"], request_params["game_date_lt"])

    # Default params is put first so that the value in request_params is preferred.
    return request_params, frequency_factor


def _get_games_in_request(request_params: ParamDict) -> pd.DataFrame:
    """
    Determine which games are included in the request.

    Based on the game_date_range, season, and season_type fields. Gets the seasons in the request first, and then gets all games that happened in those seasons and fall between the given date range.

    Args:
        request_params: The dict of the validate request query values.

    Returns:
        A DataFrame that contains every date in the request. Each date will appear the number of times that it occurs in the request.
    """

    # season and season_type are both multi-select fields, meaning the values follow the format: "value1|value2|".
    seasons_in_request: List[str] = request_params.get("hfSea").split("|")[:-1]
    season_type: List[str] = request_params.get("hfGT").split("|")[:-1]

    games_in_request: pd.DataFrame = pd.concat(
        [_get_game_logs_by_year(season, season_type) for season in seasons_in_request]
    )

    request_start_date: str = request_params.get("game_date_gt")
    request_end_date: str = request_params.get("game_date_lt")

    games_in_request: pd.DataFrame = games_in_request.query(f"'{request_start_date}' <= date <= '{request_end_date}'")

    return games_in_request


def _get_request_date_pairs(games_in_request: pd.DataFrame, frequency_factor: float) -> List[Tuple[str, str]]:
    """
    Split request into pairs of dates that will be small enough to avoid losing data.

    Since requests max out at 40,000 rows, requests must be sure to not request too much data or else data will be lost. The frequency factor is an estimate based on the parameters included in the request. For example, if a venue is included in the request, the frequency factor will be multiplied by 1/30.

    Once the number of estimated rows for the request are known, date pairs are created with each date pair representing a maximum of 35,000 estimated rows.

    Args:
        games_in_request:
        frequency_factor:

    Returns:

    """
    num_of_games: int = games_in_request["count"].sum()
    num_of_rows: float = num_of_games * PITCHES_PER_GAME * frequency_factor

    # No date pairs are needed if the request is estimated to have less than 35,000 rows initially.
    if num_of_rows <= MAX_ROWS_PER_REQUEST:
        return []

    games_per_request: float = MAX_ROWS_PER_REQUEST / (num_of_rows / num_of_games)

    dates: pd.Series = games_in_request["date"]
    counts: pd.Series = games_in_request["count"]

    date_pairs: List[Tuple[str, str]] = []
    game_count: int = 0
    request_start_date: str = dates.iloc[0]

    # Goes date by date and adds the number of games represented by those dates. Once those dates combine to have more games than the max number of games in a request, a request pair is created. Once all dates are iterated through, the final date pair is added.
    for i, (date, count) in enumerate(zip(dates, counts)):
        if game_count + count > games_per_request:
            date_pairs.append((request_start_date, dates.iloc[i - 1]))
            request_start_date = date
            game_count = count
        else:
            game_count += count

    date_pairs.append((request_start_date, dates.iloc[-1]))
    return date_pairs


def _compile_requests(request_params: ParamDict, request_date_pairs: List[Tuple[str, str]]) -> List[ParamDict]:
    """
    Create full request dicts based on all initial parameters and the list of date pairs.

    Args:
        request_params: The initial list of validated parameters.
        request_date_pairs: The list of date pairs to use.

    Returns:
        A list of dicts of requests to make. Each request will have the exact same parameters except for the date fields.
    """

    # If the date pairs are an empty list, then return just the initial parameters.
    if not request_date_pairs:
        return [request_params]

    requests_to_make: List[ParamDict] = []

    for dp in request_date_pairs:
        # Need to make a copy or else the assignments below will affect all dicts in request.
        base_request = request_params.copy()
        base_request["game_date_gt"] = dp[0]
        base_request["game_date_lt"] = dp[1]
        requests_to_make.append(base_request)

    return requests_to_make


def _get_filenames(requests_to_make: List[ParamDict]) -> List[str]:
    """
    Create filename for each request dict.

    Each filename is based on the request start and end date. If the request doesn't have a start or end date, it will look at the seasons in the request and assume the request is from 01-01 of the earliest season to 12-31 of the latest season.

    Args:
        requests_to_make: The list of request dicts.

    Returns:
        A list of filenames.
    """
    filenames: List[str] = []
    for rtm in requests_to_make:
        # If there is no start date provided, assume the request starts at 01-01 of the earliest season.
        fn_start = rtm.get("game_date_gt")
        # If there is no end date provided, assume the request starts at 12-31 of the latest season.
        fn_end = rtm.get("game_date_lt")
        filenames.append(f"{fn_start} -> {fn_end}")
    return filenames


# TODO: Drop deprecated fields, etc. here.
def _get_statcast_df(output: IO, multipart_request: bool = False) -> pd.DataFrame:
    """
    Build a DataFrame given an IO object containing csv information.

    Some additional processing is done to ensure that duplicate lines aren't included in the DataFrame. This is because when the requests are broken up, each sub-request will have a header row.

    Args:
        output: The IO object that contains the downloaded csv. Can be a file or StringIO object. Usually won't be a file because the processing to ensure non-duplicated rows should be done for the multipart requests.
        multipart_request: Whether the request was made up of more than request.

    Returns:
        A DataFrame created from the csv data and sorted by game_date.
    """

    # Need to return to the beginning of the IO stream.
    output.seek(0)

    # If the request was not built from multiple requests, there will be no duplicated rows.
    if not multipart_request:
        return pd.read_csv(output, dtype=_statcast_dtypes).drop(_deprecated_columns, axis=1)

    # Get rid of duplicated rows.
    unique_lines: List[str] = list(dict.fromkeys(output.read().split("\n")))

    # TODO: Sort dates asc or desc?
    # TODO: Figure out why dates aren't in same order each time. Since a given date only appears in one request, sorting shouldn't affect that.
    # TODO: Dropping won't work with aggregation.
    return (
        pd.read_csv(io.StringIO("\n".join(unique_lines)), dtype=_statcast_dtypes)
        .drop(_deprecated_columns, axis=1)
        .sort_values(["game_pk", "at_bat_number", "pitch_number"], ascending=False)
        .reset_index(drop=True)
    )


#
# HELPER
#


def _check_dates(date_gt: str, date_lt: str) -> None:
    start_date = datetime.strptime(date_gt, "%Y-%m-%d")
    end_date = datetime.strptime(date_lt, "%Y-%m-%d")

    if start_date < datetime.strptime("2015-03-01", "%Y-%m-%d"):
        console.print("Some metrics, such as 'exit velocity' and 'batted ball events', are not available before 2015.")

    if start_date < datetime.strptime("2008-03-25", "%Y-%m-%d"):
        raise ValueError(f"Invalid start date {date_gt}. Statcast data is only available from the 2008 season onwards.")

    if end_date >= datetime.today():
        console.print("Data is updated every day at 3 am. Some of today's data may be missing.")

    if start_date > end_date:
        raise ValueError(f"Start date '{date_gt}' is later than end date '{date_lt}'.")


# TODO: Add support for spring training?
def _get_game_logs_by_year(year: str, season_type: List[str]) -> pd.DataFrame:
    """
    Get the game logs for a given year.

    Args:
        year: The season to get game logs for.
        season_type: The season types to get game logs for (regular, postseason, both).

    Returns:
        A DataFrame of game logs for the given year.
    """
    total_game_logs: List[pd.DataFrame] = []

    for st in season_type:
        total_game_logs.append(game_logs[st].query(f"'{year}-01-01' <= date <= '{year}-12-31'"))

    return pd.concat(total_game_logs) if len(total_game_logs) > 1 else total_game_logs[0]


if __name__ == "__main__":
    # TODO: This request gives an empty file error.
    query = {
        "pitchers": ["545333", "434378", 123456],
        "batters": "123456",
        # "venue": 2395,
        "venue": 2395,
        "season": [2017, 2018, 2019],
        # "game_date_range": ["2018-07-28", "2019-06-09"],
        # "game_date_range": [None, "2019-06-09"],
        "pitch_type": ["FF", "offspeed", "slider"],
        "player_type": "batter",
        "sort_order": "asc",
        "pa_result": "home run",
        # If metric range is None, then set to max/min
        "metric_range": {"exit velocity": ["90", None], "pitch velocity": [None, 120.0]},
    }

    multi_part_query = {"season": 2019, "venue": 2395}

    game_date_test = {"game_date_range": ["2014-04-01", "2014-05-01"], "venue": "comerica park", "season": 2014}

    # make_request(query)
    res = make_request(game_date_test)
    ic(res)
