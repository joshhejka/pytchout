import json
from pathlib import Path
from typing import Dict

import pandas as pd
from rich.console import Console

from pytchout.statcast import field as fd


# TODO: Move all of this to __init__?
console = Console()
p = Path(__file__).resolve()


search_base_url: str = "https://baseballsavant.mlb.com/statcast_search/csv?"


field_types = {
    "single-select": fd.SingleSelectField,
    "multi-select": fd.MultiSelectField,
    "date-range": fd.DateRangeField,
    "player-lookup": fd.PlayerLookupField,
    "metric-range": fd.MetricRangeField,
    "as-is": fd.AsIsField,
}

statcast_fields = {}
default_params: Dict[str, str] = {"all": "true", "type": "details"}

with open(p.parent.parent.parent / "data/field_data/statcast_fields.json") as f:
    # print("DOING BASE STUFF")
    for field_type, fields in json.load(f).items():
        field_class = field_types[field_type]
        for d in fields:
            field_object = field_class(**d)
            statcast_fields[field_object.name] = field_object

            if isinstance(field_object, fd.ChoiceField) and field_object.default_choice:
                default_params.update(field_object.get_default_params())


game_logs: Dict[str, pd.DataFrame] = {
    "R": pd.read_csv(p.parent.parent.parent / "data/game_log_data/regular_season_game_logs.csv"),
    "PO": pd.read_csv(p.parent.parent.parent / "data/game_log_data/postseason_game_logs.csv"),
}
