"""
Store data for Statcast database fields.

This module provides classes that make interacting with Statcast field data easier and more intuitive.

There are five main types of Statcast fields:
    - single-select: Only one value is allowed.
    - multi-select: Multiple values are allowed.
    - date-range: A min and max date value in the format YYYY-MM-DD is allowed.
    - player-lookup: A list of valid player ids is allowed.
    - metric-range: A dict of metric names to a greater than and a less than value. Each metric name has an acceptable range of values that the greater and less than values must be between.
"""

from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import datetime
from difflib import get_close_matches
from typing import Any, Dict, List, Optional, Set, Tuple, Union

import numpy as np

from pytchout import lookup
from pytchout.statcast.types import FieldValue, ValueCollection


#
# PUBLIC
#


@dataclass(eq=False)
class Choice:
    """
    A choice for a field.

    Attributes:
        slug: The representation of the choice that appears in the request url.
        name: A common English representation of the choice.
        min_value: For metric fields, the minimum value allowed.
        max_value: For metric fields, the maximum value allowed.
        options: Set of the slug and name allowing for easy determination if a value equals either one.
    """

    slug: str
    name: str
    min_value: Optional[int] = None
    max_value: Optional[int] = None

    def __post_init__(self):
        """
        Create set of options to make for easier comparisons.

        Since the slug value and name value should both be accepted, the options attribute makes it easy to check if a given value equals one of those two values by using "if value in choice.options".
        """
        self.options: Set[str] = {self.slug.lower(), self.name.lower()}


# TODO: Decide whether field names should have underscores? Potentially also use underscores for metric field names.
@dataclass(eq=False)
class Field(ABC):
    """
    An abstract Statcast field.

    This information is scraped from the Baseball Savant Statcast search page.

    Attributes:
        name: A common English representation of the field.
        slug: The representation of the field that appears in the request url.
    """

    name: str
    slug: str

    @abstractmethod
    def get_params(self, values):
        """
        Validate values and turns them into request parameters.

        The request parameters are formatted in a way that is compatible with urllib.parse.urlencode or with requests.

        Args:
            values: The values to convert to url params.

        Returns:
            A dict of field slugs to values slugs.
        """
        pass


# TODO: Aliases should only be for MSF.
class ChoiceField(Field, ABC):
    """
    A more robust abstract Statcast field.

    Contains valid choices, aliases for selecting multiple choices at once, optional frequencies, an optional default choice, and an optional default frequency.

    This information is scraped from the Baseball Savant Statcast search page.

    Attributes:
        name: A common English representation of the field.
        slug: The representation of the field that appears in the request url.
        choices: A list of strings that are accepted values for the field.
        aliases: A list of strings that represent a list of other choices. Easy way to select multiple values at once for values that are intuitively grouped together.
        frequencies: A mapping of choice slugs to their frequency, if given.
        default_choice: A default choice slug, if given.
        default_frequency: A default frequency value, if given.
    """

    def __init__(
        self,
        name: str,
        slug: str,
        choices: List[Dict[str, Union[str, int, float]]],
        aliases: Optional[Dict[str, List[str]]] = None,
        default_choice: Optional[str] = None,
        default_frequency: float = 1.0,
    ):
        """
        Create a ChoiceField object.

        Args:
            name: A common English representation of the field.
        slug: The representation of the field that appears in the request url.
        choices: A list of strings that are accepted values for the field.
        aliases: A list of strings that represent a list of other choices. Easy way to select multiple values at once for values that are intuitively grouped together.
        default_choice: A default choice slug, if given.
        default_frequency: A default frequency value, if given.
        """
        super().__init__(name, slug)
        self.choices: List[Choice] = []
        self.aliases: Dict[str, List[str]] = {}
        self.frequencies: Dict[str, float] = {}
        self.default_choice: Optional[str] = default_choice
        self.default_frequency: float = default_frequency

        if aliases:
            for alias, values in aliases.items():
                self.aliases[alias] = values

        # Pop the frequency from the choice info and add it to the frequencies dict.
        for c in choices:
            if freq := c.pop("frequency", None):
                self.frequencies[c.get("slug")] = freq
            self.choices.append(Choice(**c))

    def get_default_params(self) -> Dict[str, str]:
        """
        Get the default choice if it exists.

        Returns:
            A dict of the field slug to the default choice slug if it exists, else the empty string.
        """
        return {self.slug: self.default_choice} if self.default_choice else {self.slug: ""}

    # TODO: Should probably just take the values themselves? Not the dictionary? The frequencies mapping only uses slugs.
    @abstractmethod
    def get_frequency_factor(self, values) -> float:
        """
        Get the frequency factor for the given list of values.

        Args:
            values: A value or list of values to get the frequency factor for.

        Returns:
            The frequency factor float.
        """
        pass

    def _get_choice_from_value(self, value: str) -> Choice:
        """
        Get the choice object associated with the value provided.

        Args:
            value: The value to get the choice from.

        Returns:
            The choice object associated with the value.

        Raises:
            InvalidChoice: If the value provided is not a valid value for any choice object.
        """
        value = str(value).lower()

        for choice in self.choices:
            if value in choice.options:
                return choice

        # TODO: Abstract to helper method?
        closest_matches = ", ".join(f"'{cm}'" for cm in get_close_matches(value, self.guide().keys(), cutoff=0))
        raise ValueError(f"'{value}' is not a valid choice for field '{self.name}'. Did you mean {closest_matches}?")

        # raise InvalidChoice(value, self)

    def guide(self) -> Dict[str, str]:
        """
        Get a string that list the valid values for the field.

        Returns:
            The description string of valid values and slugs.
        """

        return {choice.name: choice.slug for choice in self.choices}

        # string = ""
        #
        # for choice in self.choices:
        #     string += f"-> {choice.name} or {choice.slug}\n"
        #
        # if self.aliases:
        #     for key, value in self.aliases.items():
        #         string += f"-> {key} or {', '.join(value)}\n"
        #
        # return string


class SingleSelectField(ChoiceField):
    """
    A Statcast field that allows a max of one value.
    """

    def get_params(self, values: Union[str, int]) -> Dict[str, str]:
        """
        Return slug mapped to the slug of the validated choice.

        Values should be in the format:
            {
                "field_name": "string_value",
                "field_name_2": int_value
            }
        """
        return {self.slug: self._get_choice_from_value(values).slug.replace(".", r"\.")}

    def get_frequency_factor(self, values: Dict[str, str]):
        return self.frequencies.get(next(iter(values.values())), self.default_frequency)


class MultiSelectField(ChoiceField):
    """
    A Statcast field that allows multiple values.
    """

    def get_params(self, values: FieldValue) -> Dict[str, str]:
        """
        Return slug mapped to a string of validated values separated by pipes ("|") with a trailing pipe as well.

        Values should be in the format:
            {
                "field_name": "string_value",
                "field_name_2": int_value,
                "field_name_3": ["string_value", int_value]
            }
        """
        values: ValueCollection = _get_nonstring_iterable(values)
        valid_values: Set[Union[str, int]] = set()

        for value in values:
            if value in self.aliases.keys():
                valid_values.update(self.aliases[value])
            else:
                valid_values.add(self._get_choice_from_value(value).slug)

        param_values: List[str] = [v.replace(".", r"\.") for v in sorted(list(valid_values))]

        return {self.slug: f"{'|'.join(param_values)}|"}

    def get_default_params(self):
        """
        Return slug mapped to default value with a trailing pipe character.
        """
        return {self.slug: f"{self.default_choice}|"} if self.default_choice else {self.slug: ""}

    def get_frequency_factor(self, values: Dict[str, str]):
        """
        Return sum of frequency factors for each value capped at 1.0.
        """
        total: float = sum(
            [
                self.frequencies.get(value, self.default_frequency)
                for value in next(iter(values.values())).split("|")[:-1]
            ]
        )
        return total if total <= 1.0 else 1.0


class DateRangeField(Field):
    """
    A Statcast field that allows a min and max date value in the from YYYY-MM-DD.
    """

    def get_params(self, values: Union[str, List[str], Tuple[str]]) -> Dict[str, str]:
        """
        Return slug mapped to the start and end dates of the ranges.

        Values should be in the format:
            {
                "game_date_range": ["START-DATE", "END-DATE"]
            }
        """
        values = _get_nonstring_iterable(values)
        range_min, range_max = _get_range_min_and_mix(values)

        return {
            f"{self.slug}_gt": range_min if range_min and datetime.strptime(str(range_min), "%Y-%m-%d") else "",
            f"{self.slug}_lt": range_max if range_max and datetime.strptime(str(range_max), "%Y-%m-%d") else "",
        }


class PlayerLookupField(Field):
    """
    A Statcast field that allows a list of player ids.
    """

    def get_params(self, values: Union[str, List[str]]) -> Dict[str, List[str]]:
        """
        Return slug mapped to the start and end dates of the ranges.

        Values should be in the format:
            {
                "field_name": "string_value",
                "field_name_2": int_value,
                "field_name_3": ["string_value", int_value]
            }
        """
        values = _get_nonstring_iterable(values)
        valid_values: Set = set()

        for value in values:
            if not value:
                raise ValueError("Player ID cannot be None.")
            lookup.player_id_search(value)
            valid_values.add(str(value))

        return {self.slug: sorted(list(valid_values))}

    def get_frequency_factor(self, values: Dict[str, List[str]]) -> float:
        """
        Return sum of frequency factors for each value capped at 1.0.
        """
        total = 0.01 * len(next(iter(values.values())))
        return total if total <= 1.0 else 1.0


class MetricRangeField(ChoiceField):
    """
    A Statcast field that allows metric names and a min and max value for those metrics.
    """

    def get_params(self, values: Dict[str, ValueCollection]) -> Dict[str, str]:
        """
        Return slug mapped to the metric name and metric gt and lt values.

        Values should be in the format:
            {
                "metric_range": {
                    "metric_name": "single_metric_value",
                    "metric_name_2": ["metric_range_gt", "metric_range_lt"]
                }
            }
        """
        params: Dict[str, str] = {}
        num_of_metrics: int = 1

        for name, range_interval in values.items():
            choice: Choice = self._get_choice_from_value(name)

            if choice.slug in params:
                raise ValueError(f"'{choice.slug}' already in request.")

            range_interval: ValueCollection = _get_nonstring_iterable(range_interval)

            # The database expects metric values to be labeled with an incrementing counter.
            metric_number: str = f"{self.slug}_{num_of_metrics}"

            range_min: Optional[str, int]
            range_max: Optional[str, int]

            range_min, range_max = _get_range_min_and_mix(range_interval)

            # Clips values to the max or min metric value if they exist.
            if choice.min_value or choice.max_value:
                range_min: str = str(np.clip(int(range_min), choice.min_value, choice.max_value)) if range_min else ""
                range_max: str = str(np.clip(int(range_max), choice.min_value, choice.max_value)) if range_max else ""

            params[f"{metric_number}"] = choice.slug
            params[f"{metric_number}_gt"] = str(range_min)
            params[f"{metric_number}_lt"] = str(range_max)

            num_of_metrics += 1

        return params

    def get_frequency_factor(self, values: Any):
        return self.default_frequency


class AsIsField(Field):
    def get_params(self, values: Union[str, int]) -> Dict[str, str]:
        return {self.slug: str(values)}

    def get_frequency_factor(self, values: Dict[str, str]) -> float:
        return 0.001


#
# PRIVATE
#


def _get_nonstring_iterable(values: Union[str, int, ValueCollection]) -> ValueCollection:
    """
    Get a nonstring iterable given a list, tuple, or single value.

    Args:
        values: Either a single value (str or int) or a list/tuple of values.

    Returns:
        A list or tuple of str or int.
    """
    return values if isinstance(values, (list, tuple)) else [values]


def _get_range_min_and_mix(values: ValueCollection) -> Tuple[Optional[Union[str, int]], Optional[Union[str, int]]]:
    """
    Get the min and max values of a range.

    A single value will be treated as both the min and max of the range. If a single value isn't provided, then the range must be of length 2.

    Args:
        values: The single value or range of values.

    Returns:
        A tuple of length 2 representing the min and max values. None is allowed as a value.
    """

    # A single value will be considered as the min and max of the range.
    if len(values) == 1:
        return (values[0], values[0]) if values[0] else (None, None)
    # Ranges must be of length 2, a min and a max.
    elif len(values) != 2:
        raise ValueError("Invalid range format provided. Format should look like: '[start_value, end_value]'.")

    return values[0] if values[0] else None, values[1] if values[1] else None
