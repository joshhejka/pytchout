from concurrent.futures import ThreadPoolExecutor
from contextlib import closing
from typing import List, Dict, IO, Optional

import requests
from rich.progress import (
    BarColumn,
    DownloadColumn,
    TextColumn,
    TransferSpeedColumn,
    TimeRemainingColumn,
    Progress,
    TaskID,
)


progress: Progress = Progress(
    TextColumn("[blue]{task.fields[filename]}", justify="right"),
    BarColumn(bar_width=None),
    "[progress.percentage]{task.percentage:>3.1f}%",
    "•",
    DownloadColumn(),
    "•",
    TransferSpeedColumn(),
    "•",
    TimeRemainingColumn(),
)


# TODO: Turn into one method for downloading multiple and one for downloading one?
def download_csv(
    url: str, output, params_list: Optional[List[Dict]] = None, filenames: Optional[List[str]] = None
) -> None:
    params_list = [{}] if not params_list else params_list
    filenames = ["csv"] * len(params_list) if not filenames else filenames

    with progress, ThreadPoolExecutor(max_workers=5) as pool:
        for p, filename in zip(params_list, filenames):
            task_id: TaskID = progress.add_task("download", filename=filename, start=False, visible=False)
            # TODO: Check for requests.exceptions.SSLError here.
            pool.submit(_make_request, task_id, url, output, p)


def _make_request(task_id: TaskID, url: str, output: IO, params: Dict) -> IO:
    progress.update(task_id, visible=True)
    with closing(requests.get(url, params, stream=True)) as r:
        length: int = int(r.headers["content-length"])
        progress.start_task(task_id)
        progress.update(task_id, total=length)
        # for line in r.iter_lines(32768):
        # TODO: Is decoding faster in the iterator or below?
        for line in r.iter_lines(decode_unicode=True):
            # output.write(line.decode("utf-8") + "\n")
            output.write(line + "\n")
            progress.update(task_id, advance=len(line))
        progress.update(task_id, completed=length)
    return output
