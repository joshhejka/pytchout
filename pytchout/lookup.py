import io
from contextlib import closing
from difflib import get_close_matches
from pathlib import Path
from typing import Optional, Union

import pandas as pd
import requests
from icecream import ic
from loguru import logger

import pytchout.downloader as dl


sources = ["mlbam", "fangraphs", "bbref", "retrosheet"]
lookup_columns = [
    "name_last",
    "name_first",
    "key_mlbam",
    "key_retro",
    "key_bbref",
    "key_bbref_minors",
    "key_fangraphs",
    "mlb_played_first",
    "mlb_played_last",
]

# TODO: Look into why "content-length" is incorrect here.
lookup_url = "https://raw.githubusercontent.com/chadwickbureau/register/master/data/people.csv"

_lookup_dtypes = {
    "key_mlbam": pd.Int64Dtype(),
    "key_fangraphs": pd.Int64Dtype(),
    "mlb_played_first": pd.Int64Dtype(),
    "mlb_played_last": pd.Int64Dtype(),
}


def get_lookup_table(refresh: bool = False) -> pd.DataFrame:
    p = Path(__file__).resolve().parent.parent / "data/lookup_data/chadwick.csv"

    if not p.exists() or refresh:
        with io.StringIO() as output:
            dl.download_csv(lookup_url, output, filenames=["lookup table"])

            output.seek(0)
            data = (
                pd.read_csv(output, usecols=lookup_columns, dtype=_lookup_dtypes)
                .dropna(subset=lookup_columns[2:])
                .reset_index(drop=True)
            )

            data.to_csv(p)

            return data
    else:
        return pd.read_csv(p, dtype=_lookup_dtypes, index_col=0)


def player_id_search(
    player_id: Union[int, float, str], source: str = "mlbam", *, refresh: bool = False
) -> Optional[str]:
    if source not in sources:
        raise ValueError(f"Invalid source provided. Source must be one of {', '.join(sources)}.")

    data: pd.DataFrame = get_lookup_table(refresh)
    player_row = data.loc[data[f"key_{source}"] == int(player_id)]

    # TODO: Add refresh here. Maybe abstract away to helper function and use below also.
    if player_row.empty:
        raise ValueError(
            f"Invalid player id '{player_id}' provided. Use 'pytchout.lookup.player_id_search()' to get IDs for players by name. If you think the lookup table may be out of date, you can refresh it by including 'refresh=True' in your function call."
        )

    return player_row

    # player_name = f"{player_row['name_first'].item()} {player_row['name_last'].item()}"
    #
    # return player_name


def player_name_search(
    last_name: str, first_name: str, source: str = "mlbam", *, refresh: bool = False
) -> Optional[int]:
    if source not in sources:
        raise ValueError(f"Invalid source provided. Source must be one of {', '.join(sources)}.")

    data: pd.DataFrame = get_lookup_table(refresh)

    # TODO: Deal with repeat names like Joe Smith.
    # TODO: "Contains" may cause issues with names like Chris -> Christopher?
    player_row = data.loc[
        (data["name_last"].str.contains(last_name, case=False))
        & (data["name_first"].str.contains(first_name, case=False))
    ]

    if player_row.empty:
        logger.warning(
            f"Player '{first_name} {last_name}' not found. If you think the lookup table may be out of date, you can refresh it by including 'refresh=True' in your function call."
        )

        # closest_ln = get_close_matches(last_name.lower(), data["name_last"].dropna().str.lower())[0]
        #
        # closest_fn = get_close_matches(first_name.lower(), data["name_first"].dropna().str.lower())[0]
        #
        # player_row = data.loc[
        #     (data["name_last"].str.contains(closest_ln, case=False))
        #     & (data["name_first"].str.contains(closest_fn, case=False))
        # ]

        # if player_row.empty:
        #     return None

    return player_row

    # return player_row[f"key_{source}"].item()


# def playerid_lookup():
#     pass
#
#
# def playerid_reverse_lookup():
#     pass


if __name__ == "__main__":
    # bauer_name = player_id_search(123456, source="mlbam", refresh=True)
    # print(bauer_name)

    # bauer_id = player_name_search("oday", "darren", source="mlbam")
    # print(bauer_id)

    bauer = player_id_search(545333, source="mlbam")
    print(bauer)

    oday = player_name_search("o'day", "darren", source="mlbam")
    print(oday)

    repeat_id = player_name_search("smith", "joe", source="mlbam")
    print(repeat_id)
